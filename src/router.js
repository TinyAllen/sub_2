import Vue from 'vue'
import Router from 'vue-router'
import About from './views/Home.vue'

Vue.use(Router)

export default [
  {
    path: '/about',
    name: 'About',
    component: About
  },
]
